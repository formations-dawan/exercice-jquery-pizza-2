# Préparation
Placez le dossier *images* et *json* à la racine de votre site.

# Consignes
Afficher un menu de pizzas, récupérées en AJAX depuis le fichier `./json/pizzas.json`.

Stylisez à votre façon votre page HTML, utilisez des animations en jQuery, et amusez-vous ;)