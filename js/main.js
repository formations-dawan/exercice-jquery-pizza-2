function conversionJsonHtmlPizzas(listePizzas) {
    let html = "";

    for (pizza of listePizzas) {
        html += `<article class="pizza diet-${pizza.category}">`;
        html +=     `<img src="${pizza.image}" alt="Une pizza :)" width="100px" height="100px"></img>`;
        html +=     `<h1>${pizza.name}</h1>`;
        html +=     `<p>${pizza.description}</p>`;
        html +=     `<h2>Ingrédients</h2>`;
        html +=     `<ul>`;

        for (let ingredient of pizza.ingredients) {
            html +=     `<li>${ingredient}</li>`;
        }

        html +=     `</ul>`;
        html +=     `<h2>Prix</h2>`;
        html +=     '<p>' + pizza.price + '</p>'; // `<p>${pizza.price}</p>`
        html +=  '</article>';
    }

    return html;
}

jQuery(function () {
    const url = "./json/pizzas.json";

    $.get(
        url,
        function (response) {
            $('main').html(conversionJsonHtmlPizzas(response));
            gestionEvenementImg();
        },
        "json"
    )
    .fail(function (error) {
        $('main').html('<p>Aucune pizza dispo :(</p>');
    });

    $('main').html('<div class="loader"></div>'); //loader AJAX (classe à définir)

    function gestionEvenementImg() {
        /* 
        * $('article > img').on('mouseenter', () => { ...
            --> "this" non disponible avec une fonction arrow (flechée)
        */
        $('article > img').on('mouseenter', function () { 
            $(this).animate({
                width: "150px",
                height: "150px"
            },
            "slow"
            );
        });

        $('article > img').on('mouseleave', function () {
            $(this).animate({
                width: "100px",
                height: "100px"
            },
            "slow"
            );
        });
    }
});